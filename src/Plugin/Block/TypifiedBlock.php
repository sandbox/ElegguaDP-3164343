<?php

namespace Drupal\typified_blocks\Plugin\Block;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a 'Typified' block.
 *
 * @Block(
 *   id = "typified_block",
 *   admin_label = @Translation("Typified Block"),
 * )
 */
class TypifiedBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Allowed types.
   *
   * @var array
   */
  protected $allowedTypes = [];

  /**
   * The Plugin Block Manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Constructs a new TypifiedBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The Plugin Block Manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param array $allowed_types
   *   Allowed types.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BlockManagerInterface $block_manager, EntityTypeManagerInterface $entity_type_manager, array $allowed_types, EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->blockManager = $block_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->allowedTypes = $allowed_types;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.block'),
      $container->get('entity_type.manager'),
      _typified_blocks__allowed_values__field_type(),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'type' => '',
      'view_mode' => 'full',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['type'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Block Type'),
      '#options' => $this->allowedTypes,
      '#default_value' => $this->configuration['type'],
    ];

    $view_modes = [];

    foreach ($this->entityDisplayRepository->getViewModes('block_content') as $mode) {
      $view_modes[explode('.', $mode['id'])[1]] = $mode['label'];
    }

    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#options' => $view_modes,
      '#default_value' => $this->configuration['view_mode'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $this->configuration['type'] = $form_state->getValue('type');
    $this->configuration['view_mode'] = $form_state->getValue('view_mode');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block = [];

    $block_id = \Drupal::entityQuery('block_content')
      ->condition('field_type', $this->configuration['type'])
      ->condition('status', 1)
      ->accessCheck(FALSE)
      ->execute();

    if (!empty($block_id)) {
      $block_entity = $this->entityTypeManager->getStorage('block_content')->load(reset($block_id));

      if ($block_entity) {
        $block = $this->entityTypeManager
          ->getViewBuilder($block_entity->getEntityTypeId())
          ->view($block_entity, empty($this->configuration['view_mode']) ? 'full' : $this->configuration['view_mode']);
      }
    }

    return $block;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['user', 'url.path']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return array_merge(parent::getCacheTags(), [
      'typified-block',
      'typified-block-' . $this->configuration['type'],
    ]);
  }

}
