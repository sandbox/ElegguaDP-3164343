<?php

namespace Drupal\typified_blocks\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Displays the popup type settings form.
 */
class TypifiedBlockSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'typified_blocks_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['types'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Block types'),
      '#rows' => 5,
      '#default_value' => implode("\n", (array) $this->config('typified_blocks.settings')->get('types')),
      '#description' => $this->t('Enter one by line block types.'),
      '#wysiwyg' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $values = str_replace([
      "\r\n",
      "\r",
    ], "\n", $form_state->getValue('types'));

    if (!in_array('info', explode(PHP_EOL, $values))) {
      $form_state->setErrorByName('types', $this->t('"Info" is default type.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = str_replace([
      "\r\n",
      "\r",
    ], "\n", $form_state->getValue('types'));

    $this->config('typified_blocks.settings')
      ->set('types', explode(PHP_EOL, $values))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['typified_blocks.settings'];
  }

}
