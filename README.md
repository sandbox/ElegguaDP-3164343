# Typified Blocks

The Typified Blocks module helps you to place custom content blocks into the Block layout
without UUID dependency by new configurable block "Typified Block" with predefined "types".

## Requirements

This module requires the following modules:

 * Custom Block (Drupal core)
 * Options (Drupal core)

## Installation

1. Install module as usually.
2. Add/update Typified Block types on `/admin/config/user-interface/typified-blocks/block-types`

    **N.B.: `info` type is predefined type and can not be removed or changed.**

## Usage

1. Add types for your typified blocks on `/admin/config/user-interface/typified-blocks/block-types`
2. Place into the Block layout section new Typified block with defined type.
3. Add to your custom block content type new "field_type" field from list of the existing fields.
4. Create/update custom block content with same type

    **N.B.: You can use only one type per block.**
    
5. Your custom block content will be rendered in the specified Block layout section as common content block.
